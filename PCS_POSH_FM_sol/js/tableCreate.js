/**
 * Created by isaac on 5/17/16.
 */
var tableCreate = {

    table: null,

    setTable: function (def) {

        if (def.id == "") {
            this.table = $('<table></table>');
        } else {
            this.table = $('#' + def.id);
        }
    },

    create: function (def, bodyData, expandable, startExpanded) {


        /* if expandable == true, 
           then make the entire expandable, showing first line only
           Default: not expandable, just a regular table
        
           if startExpanded,
           then start expanded (showing all rows)
           Defauls, start showing first line only.
        */

        expandable = expandable || false;
        startExpanded = startExpanded || false;


        this.setTable(def);
        this.buildHeader(def.id, def.tableClass, def.defs.headers);
        this.addBody(def.id, bodyData);

        if (def.id == "") {
            return this.table.html();
        }
    },

    buildHeader: function (targetId, tableClass, headers) {

        var table = this.table.addClass(tableClass);

        //var table = $('#'+ targetId).addClass(tableClass);

        var thead = $('<thead></thead>');

        $(headers).each(function () {

            var row = $('<tr></tr>');
            var dataRow = $(this);
            $(dataRow).each(function () {
                var dataTH = $(this)[0];

                var th = $('<th></th>').addClass(dataTH.class).html(dataTH.title).prop("colspan", dataTH.colspan).prop("rowspan", dataTH.rowspan);
                row.append(th);
            });

            thead.append(row);
        })
        table.append(thead);
        this.table = table;
    },

    addBody: function (targetId, bodyData) {
        var table = this.table;

        $(bodyData).each(function () {

            var rowData = $(this);

            var row = $('<tr></tr>');
            rowData.each(function () {

                var td = $('<td></td>').html(this);
                row.append(td);
            })
            table.append(row);

        });
        this.table = table;
    },
};


function newExpandableTable(){
    var expandableTable = {
        table: null,

        firstRow: null,
        commandRow: null,
        body: null,
        expanded: true,

        setTable: function (table) {
            this.table = table;

            // make header clickable
            if (this.table.children('thead').length > 0) {
                this.table.children('thead').addClass('expandable');
            } else {
                this.table.children('tr').addClass('expandable');
            }


            // find first row
            if (this.table.children('tbody').length > 0) {
                this.firstRow = this.table.children('tbody').children().first();
            } else {
                this.table.children().find('td').first().parent();
            }

            // find rest of the body to be hidden or shown
            this.body = this.table.children('tbody').children('tr').not(this.firstRow);

            if (this.body.length == 0) {
                this.body = this.table.children().find('td').parent().not(this.firstRow);
            };


            // add a command row
            var row = $('<tr>');
            var cell = $('<td>');
            cell.prop('colspan', this.firstRow.children().length);
            row.append(cell);
            row.addClass('expandable commandLine');
            this.commandRow = row;
            this.table.children('tbody').append(row);

            this.setCommandText();

            var that = this;

            $(this.table).on('click', '.expandable', function () {

                if (that.isExpanded()) {
                    that.compress();
                } else {
                    that.expand();
                }
            });

            if (!this.isExpanded()) {
                this.compress();
            }

            return this.table;
        },

        setCommandText: function () {
            if (this.isExpanded()) {
                this.table.find('.commandLine').children().first().html('less...');
            } else {
                this.table.find('.commandLine').children().first().html('more...');
            }
        },

        isExpanded: function () {
            return this.expanded;
        },

        expand: function () {
            this.body.show();
            this.expanded = true;
            this.setCommandText();
        },

        compress: function () {
            this.body.hide();
            this.expanded = false;
            this.setCommandText();
        }
    };

    return expandableTable;
}

