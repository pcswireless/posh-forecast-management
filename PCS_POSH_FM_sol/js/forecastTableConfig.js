var forecastTableSignificantChange = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];

var forecastTableConfig = {
    "id": "tablePlaceholder",
    "tableClass": "tblForecast",
    "defs": {
        "headers": [
            [
                {
                    "title": "Item",
                    "colspan": 1,
                    "rowspan": 2
                },
                {
                    "title": "History",
                    "colspan": 3
                },
                {
                    "title": "Committed",
                    "colspan": 4
                },
                {
                    "title": "Forecast",
                    "colspan": 10
                },
                {
                    "title": "14 week<br>total",
                    "colspan": 1,
                    "rowspan": 2
                }
            ],
            [
                {
                    "title": "W-3"
                },
                {
                    "title": "W-2"
                },
                {
                    "title": "W-1"
                },
                {
                    "title": "W1"
                },
                {
                    "title": "W2"
                },
                {
                    "title": "W3"
                },
                {
                    "title": "W4"
                },
                {
                    "title": "W5"
                },
                {
                    "title": "W6"
                },
                {
                    "title": "W7"
                },
                {
                    "title": "W8"
                },
                {
                    "title": "W9"
                },
                {
                    "title": "W10"
                },
                {
                    "title": "W11"
                },
                {
                    "title": "W12"
                },
                {
                    "title": "W13"
                },
                {
                    "title": "W14"
                }
            ]
        ]
    }
};