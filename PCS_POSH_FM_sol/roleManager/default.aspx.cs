﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PoshForecastingTool.roleManager
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.Master.FindControl("lnkEdit").Visible = false;
            HyperLink btn = (HyperLink) Master.Master.FindControl("lnkHome");
            btn.CssClass += " selected";
        }
    }
}