/**
 * Created by isaac on 5/17/16.
 */
var tableCreate = {

    table: null,

    setTable: function (def) {

        if (def.id == "") {
            this.table = $('<table></table>');
        } else {
            this.table = $('#' + def.id);
        }
    },

    create: function (def, bodyData) {

        this.setTable(def);
        debugger;
        this.buildHeader(def.id, def.tableClass, def.defs.headers);
        this.addBody(def.id, bodyData);

        if (def.id == "") {
            return this.table.html();
        }
    },

    buildHeader: function (targetId, tableClass, headers) {

        var table = this.table.addClass(tableClass);

        //var table = $('#'+ targetId).addClass(tableClass);

        var thead = $('<thead></thead>');

        $(headers).each(function () {

            var row = $('<tr></tr>');
            var dataRow = $(this);
            $(dataRow).each(function () {
                var dataTH = $(this)[0];

                var th = $('<th></th>').addClass(dataTH.class).html(dataTH.title).prop("colspan", dataTH.colspan).prop("rowspan", dataTH.rowspan);
                row.append(th);
            });

            thead.append(row);
        })
        table.append(thead);
        this.table = table;
    },

    addBody: function (targetId, bodyData) {
        var table = this.table;

        $(bodyData).each(function () {

            var rowData = $(this);

            var row = $('<tr></tr>');
            rowData.each(function () {

                var td = $('<td></td>').html(this);
                row.append(td);
            })
            table.append(row);

        });
        this.table = table;
    }

}