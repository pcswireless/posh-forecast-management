﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Script.Serialization;

namespace PoshForecastingTool.ajax
{
    public class productionPlanData : IHttpHandler
    {

        public class RowData
        {
            public List<string> cellValues { get; set; }
        }

        public class TableData
        {
            public List<RowData> rows { get; set; }
        }


        private TableData buildDummyData()
        {
            TableData tableData = new TableData();
            tableData.rows = new List<RowData>();

            RowData rowData;

            Random rnd = new Random();
            for (int row = 0; row < 100; row++)
            {

                // build table row data
                rowData = new RowData();
                rowData.cellValues = new List<string>();

                for (int col = 0; col <= 8; col++)
                {
                    // build cell (row,col) data
                    string cellValue = row.ToString() + col.ToString() + rnd.Next(0, 99);
                    if (col == 0)
                    {
                        cellValue = "Item " + row;
                    }
                    rowData.cellValues.Add(cellValue);
                }

                tableData.rows.Add(rowData);
            }
            return tableData;
        }







        public void ProcessRequest(HttpContext context)
        {
            TableData data = buildDummyData();

            context.Response.ContentType = "application/json";
            var json = new JavaScriptSerializer();
            string jsonString = json.Serialize(data);

            context.Response.Write(jsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}